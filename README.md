#  URL Shortener


## Description
This project creates service for URL Shortener

### Functionality:
- Registration Web address via REST API
- Redirect client in accordance with the shortened URL
- Registration Web address via Web Form


## API contract

### 1. Add new URL
| Attribute     | Value          |
|-------------- | -------------- |
| HTTP method   | POST           |
| URI           | **/add**       |
| Request type  | application/json                                 |
| Request       | json object w/ full url (see example below)      |
| Response type | application/json                                 |
| Response      | json object w/ short url (see example below)     |

##### Add new URL request example
```json
{
   "fullUrl" : "http://apple.com"
}
```

##### Add new URL response example
```json
{
   "shortUrl" : "http://shortener:8080/42"
}
```

### 2. Redirection
| Attribute        | Value            |
| ---------------- | ---------------- |
| HTTP method      | GET              |
| URI              | **/${shortUrl}** |
| Response status  | redirection (`301`) and new "Location" header **-OR-** Not Found (`404`) |
| Response type    | text/html                                                                |
| Response         | requested page **-OR-** empty body (`404`)                               |

### 3. Web form
| Attribute        | Value          |
| ---------------- | -------------- |
| HTTP method      | GET            |
| URI              | **/**          |
| Response type    | text/html      |
| Response         | form w/ controls for shortener |


## Configuration
- Application can be configured using property file (see _application.yml_):
   - `server.port` - port for app
   - `app.service.baseUrl` - root path for service URI
   - `storage.metaData` - location for DB
   - `spring.h2.*` and `spring.datasource.*` - standard option for DB configuration


## Usage / Deployment

### Run via Gradle
1. You can run the application using `./gradlew bootRun`

### Build & Run
1. You can build the JAR file using `./gradlew build` 
1. Then you can run the JAR file: `java -jar shortener-rs/build/libs/shortener-rs-1.0.jar`


## Implementation Details
The solution is implemented using:
1. **Spring Boot** framework
1. Embedded **H2** DB as a storage for url mapping
1. **Lombok** library to get rid of boilerplate code
1. **Gradle** as a build system


## Assumptions

### "Generation of ID" instead of "using unified sequence"
- It's faster
- It's cheaper
- It's easier to scale (add new node w/o any doubt)
- But... There is a slight chance of a cache collision, so

#### Alternatives
- "Unified" sequence in DB
- Special service (eg, based on "AtomicInteger")

#### Note
- The current solution contains a separate component for generating ID, so if necessary it's possible to use any of alternatives

### REST API vs Web Form
- To simplify and speed up the development is recommended to use **REST API** and extract all UI part in separate project; this will allow:
   - divide logic (and other back-end specific things) and display (front-end)
   - support several different types of clients (web / mobile)
   - use several Dev-teams