package com.apple.shortener.service.lib;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UrlFormatterAndValidator.class)
@TestPropertySource(properties = {
    "app.service.baseUrl=not_localhost",
    "server.port=7777"
})
public class UrlFormatterAndValidatorTest {

  @Autowired
  private UrlFormatterAndValidator service;

  @Test
  public void formatUrl() throws Exception {
    String result = service.formatUrl("42");

    assertEquals("http://not_localhost:7777/42", result);
  }

}