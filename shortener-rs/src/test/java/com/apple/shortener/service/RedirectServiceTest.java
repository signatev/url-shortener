package com.apple.shortener.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import com.apple.shortener.dao.UrlMappingRepository;
import com.apple.shortener.dao.entity.UrlMetaData;
import com.apple.shortener.service.model.RedirectEntry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RedirectServiceTest {

  @InjectMocks
  private RedirectService service;

  @Mock
  private UrlMappingRepository urlMappingRepository;


  @Test
  public void redirectedEntry() {
    UrlMetaData responseFromDao = UrlMetaData.builder()
        .shortUrl("42")
        .fullUrl("http://apple.com")
        .build();
    when(urlMappingRepository.findOne((anyString())))
        .thenReturn(responseFromDao);

    RedirectEntry result = service.processRedirect("42");

    assertEquals(responseFromDao.getFullUrl(), result.getFullUrl());
  }

}