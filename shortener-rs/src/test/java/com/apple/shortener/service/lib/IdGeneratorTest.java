package com.apple.shortener.service.lib;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class IdGeneratorTest {
  private IdGenerator idGenerator = new IdGenerator();

  @Test
  public void generateCommonMd5() throws Exception {
    assertEquals("9e107d9d372bb6826bd81d3542a419d6",
        idGenerator.generate("The quick brown fox jumps over the lazy dog"));
    assertEquals("e4d909c290d0fb1ca068ffaddf22cbd0",
        idGenerator.generate("The quick brown fox jumps over the lazy dog."));
    assertEquals("d41d8cd98f00b204e9800998ecf8427e",
        idGenerator.generate(""));
  }

}