package com.apple.shortener.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.apple.shortener.dao.UrlMappingRepository;
import com.apple.shortener.dao.entity.UrlMetaData;
import com.apple.shortener.service.lib.IdGenerator;
import com.apple.shortener.service.lib.UrlFormatterAndValidator;
import com.apple.shortener.service.model.ShortenerEntry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ShortenerServiceTest {

  @InjectMocks
  private ShortenerService service;

  @Mock
  private UrlMappingRepository urlMappingRepository;

  @Mock
  private IdGenerator idGenerator;

  @Mock
  private UrlFormatterAndValidator urlFormatterAndValidator;


  @Test
  public void addNewUrl() throws Exception {
    when(idGenerator.generate(anyString()))
        .thenReturn("42");
    when(urlFormatterAndValidator.formatUrl(anyString()))
        .thenReturn("http://formatted.url");

    ShortenerEntry result = service.add("http://apple.com");

    verify(urlMappingRepository, times(1))
        .save((UrlMetaData) anyObject());
    assertEquals("http://formatted.url", result.getShortUrl());
  }

}