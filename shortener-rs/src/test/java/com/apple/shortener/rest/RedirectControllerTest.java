package com.apple.shortener.rest;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


import com.apple.shortener.service.RedirectService;
import com.apple.shortener.service.model.RedirectEntry;
import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RedirectController.class)
public class RedirectControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;

  @MockBean
  RedirectService service;

  @Test
  public void foundUrl() throws Exception {
    when(service.processRedirect(any()))
        .thenReturn(new RedirectEntry("http://full.Test.Url"));

    mockMvc.perform(get("/shortTestUrl")
        .contentType(APPLICATION_JSON))
        .andExpect(status().isFound())
        .andExpect(header().string("Location", "http://full.Test.Url"));
  }

  @Test
  public void notFoundUrl() throws Exception {
    when(service.processRedirect(any()))
        .thenReturn(null);

    mockMvc.perform(get("/shortTestUrl")
        .contentType(APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

}