package com.apple.shortener.rest;


import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import com.apple.shortener.rest.dto.ShortUrlRequest;
import com.apple.shortener.rest.dto.ShortUrlResponse;
import com.apple.shortener.service.ShortenerService;
import com.apple.shortener.service.model.ShortenerEntry;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(ShortenerController.class)
public class ShortenerControllerTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  ObjectMapper objectMapper;

  @MockBean
  ShortenerService service;


  @Test
  public void addUrl() throws Exception {
    when(service.add(any()))
        .thenReturn(new ShortenerEntry("shortTestUrl"));

    mockMvc.perform(post("/add")
        .content(generateRequest("http://full.Test.Url"))
        .contentType(APPLICATION_JSON))
        .andExpect(status().isCreated())
        .andExpect(content().string(generateResponse("shortTestUrl")));
  }

  @SneakyThrows
  private String generateRequest(String fullUrl) {
    return objectMapper.writeValueAsString(new ShortUrlRequest(fullUrl));
  }

  @SneakyThrows
  private String generateResponse(String shortUrl) {
    return objectMapper.writeValueAsString(new ShortUrlResponse(shortUrl));
  }
}