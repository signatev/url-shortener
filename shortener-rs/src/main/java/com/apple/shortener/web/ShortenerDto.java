package com.apple.shortener.web;

import lombok.Data;

@Data
public class ShortenerDto {

  String fullUrl;

  String shortUrl;

}
