package com.apple.shortener.web;

import com.apple.shortener.service.ShortenerService;
import com.apple.shortener.service.model.ShortenerEntry;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Slf4j
@Controller
@EnableWebMvc
public class WebController {

  private final ShortenerService shortenerService;

  public WebController(
      @Autowired ShortenerService shortenerService
  ) {
    this.shortenerService = shortenerService;
  }

  @GetMapping("/")
  public String shortenerForm(Model model) {
    model.addAttribute("shortenerDto", new ShortenerDto());
    return "index";
  }

  @PostMapping("/")
  public String shortenerSubmit(@ModelAttribute ShortenerDto shortenerDto) {
    ShortenerEntry result = shortenerService.add(shortenerDto.getFullUrl());
    shortenerDto.setShortUrl(result.getShortUrl());

    log.info("Create short link '{}' for '{}'", result.getShortUrl(), shortenerDto.getFullUrl());

    return "result";
  }

}
