package com.apple.shortener.service.lib;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class UrlFormatterAndValidator {

  @Value("${app.service.baseUrl:localhost}")
  private String baseServiceUrl;

  @Value("${server.port:8080}")
  private int servicePort;

  /**
   * Construct pretty short url (w/ schema, port and host name).
   */
  public String formatUrl(String shortUrl) {
    StringBuilder sb = new StringBuilder()
        .append("http://")
        .append(baseServiceUrl);
    if (servicePort != 80) {
      sb.append(":").append(servicePort);
    }
    sb.append("/").append(shortUrl);

    return sb.toString();
  }

  /**
   * Validate url format and structure.
   */
  public boolean validateUrl(String url) {
    // TODO(isv): implement dis
    return true;
  }

}
