package com.apple.shortener.service;


import com.apple.shortener.dao.UrlMappingRepository;
import com.apple.shortener.dao.entity.UrlMetaData;
import com.apple.shortener.service.lib.IdGenerator;
import com.apple.shortener.service.lib.UrlFormatterAndValidator;
import com.apple.shortener.service.model.ShortenerEntry;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class ShortenerService {

  private final UrlMappingRepository urlMappingRepository;

  private final UrlFormatterAndValidator urlHelper;

  private final IdGenerator idGenerator;

  public ShortenerService(
      @Autowired UrlMappingRepository urlMappingRepository,
      @Autowired UrlFormatterAndValidator urlFormatterAndValidator,
      @Autowired IdGenerator idGenerator
  ) {
    this.urlMappingRepository = urlMappingRepository;
    this.urlHelper = urlFormatterAndValidator;
    this.idGenerator = idGenerator;
  }

  /**
   * Add url to system.
   *
   * @param fullUrl to add.
   * @return ShortenerEntry with short url.
   */
  public ShortenerEntry add(String fullUrl) {
    String shortUrl = idGenerator.generate(fullUrl);
    UrlMetaData urlMetaData = UrlMetaData.builder()
        .shortUrl(shortUrl)
        .fullUrl(fullUrl)
        .build();

    log.info("Adding link '{}' -> '{}' to db", urlMetaData.getShortUrl(), urlMetaData.getFullUrl());
    urlMappingRepository.save(urlMetaData);

    return new ShortenerEntry(urlHelper.formatUrl(urlMetaData.getShortUrl()));
  }

}
