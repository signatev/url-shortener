package com.apple.shortener.service.lib;

import lombok.SneakyThrows;

import org.springframework.stereotype.Component;

import java.security.MessageDigest;

@Component
public class IdGenerator {

  // TODO(isv): try different ID strategy (read value from DB seq / use Atomic?)

  /**
   * Generates ID (short url) based on url hash.
   */
  @SneakyThrows
  public String generate(String url) {
    // common md5 implementation
    MessageDigest md = MessageDigest.getInstance("MD5");
    md.update(url.getBytes());
    byte[] bytes = md.digest();
    StringBuilder sb = new StringBuilder();
    for (byte aByte : bytes) {
      sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

}
