package com.apple.shortener.service;

import com.apple.shortener.dao.UrlMappingRepository;
import com.apple.shortener.dao.entity.UrlMetaData;
import com.apple.shortener.service.model.RedirectEntry;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class RedirectService {

  private final UrlMappingRepository urlMappingRepository;

  public RedirectService(
      @Autowired UrlMappingRepository urlMappingRepository
  ) {
    this.urlMappingRepository = urlMappingRepository;
  }

  /**
   * Gets redirect entry for given short url.
   *
   * @param shortUrl to process.
   * @return RedirectEntry with full url OR null if not found such short url.
   */
  public RedirectEntry processRedirect(String shortUrl) {
    UrlMetaData urlMetaData = urlMappingRepository.findOne(shortUrl);
    if (urlMetaData != null) {
      log.info("Returning saved value for '{}' -> '{}'", urlMetaData.getShortUrl(), urlMetaData.getFullUrl());
      return new RedirectEntry(urlMetaData.getFullUrl());
    }

    log.warn("Not found value for '{}' in db", shortUrl);
    return null;
  }

}
