package com.apple.shortener.service.model;

import lombok.NonNull;
import lombok.Value;

@Value
public class ShortenerEntry {

  @NonNull
  String shortUrl;

}
