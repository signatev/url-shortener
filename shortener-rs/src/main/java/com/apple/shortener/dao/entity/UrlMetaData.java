package com.apple.shortener.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Representation of table for storing url mapping (short->full).
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UrlMetaData implements Serializable {

  private static final long serialVersionUID = 5040365779828551386L;

  @Id
  private String shortUrl;

  @Column(nullable = false)
  private String fullUrl;

}
