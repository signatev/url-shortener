package com.apple.shortener.dao;

import com.apple.shortener.dao.entity.UrlMetaData;

import org.springframework.data.repository.CrudRepository;

/**
 * Url Mapping Repository responsible for saving url mapping (short->full).
 */
public interface UrlMappingRepository extends CrudRepository<UrlMetaData, String> {
}
