package com.apple.shortener.rest;

import com.apple.shortener.rest.dto.ShortUrlRequest;
import com.apple.shortener.rest.dto.ShortUrlResponse;
import com.apple.shortener.service.ShortenerService;
import com.apple.shortener.service.model.ShortenerEntry;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class ShortenerController {

  private final ShortenerService shortenerService;

  public ShortenerController(
      @Autowired ShortenerService shortenerService
  ) {
    this.shortenerService = shortenerService;
  }


  @RequestMapping(path = "/add", method = RequestMethod.POST)
  public ResponseEntity<ShortUrlResponse> add(
      @RequestBody ShortUrlRequest requestDto
  ) {
    ShortenerEntry result = shortenerService.add(requestDto.getFullUrl());
    log.info("Create short link '{}' for '{}'", result.getShortUrl(), requestDto.getFullUrl());

    return new ResponseEntity<>(new ShortUrlResponse(result.getShortUrl()), HttpStatus.CREATED);
  }

}