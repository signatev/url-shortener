package com.apple.shortener.rest;

import com.apple.shortener.service.RedirectService;
import com.apple.shortener.service.model.RedirectEntry;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class RedirectController {

  private final RedirectService redirectService;

  public RedirectController(
      @Autowired RedirectService redirectService
  ) {
    this.redirectService = redirectService;
  }


  @RequestMapping(method = RequestMethod.GET, path = "/{shortUrl}")
  public ResponseEntity<String> redirect(@PathVariable("shortUrl") String shortUrl) {
    RedirectEntry result = redirectService.processRedirect(shortUrl);

    if (result != null) {
      HttpHeaders headers = new HttpHeaders();
      headers.add("Location", result.getFullUrl());

      log.info("Redirect '{}' to '{}'", shortUrl, result.getFullUrl());
      return new ResponseEntity<>(headers, HttpStatus.FOUND);
    } else {
      log.warn("Redirect for '{}' failed, url not found", shortUrl);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

}
