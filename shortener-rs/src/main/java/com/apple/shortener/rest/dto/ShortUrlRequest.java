package com.apple.shortener.rest.dto;

import lombok.Value;

@Value
public class ShortUrlRequest {

  String fullUrl;

}
