/*
 * Copyright (c) 2017 Blah-blah. All rights reserved.
 */

package com.apple.shortener;


import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class App {
  public static void main(String[] args) {
    log.info("Start URL shortener");
    SpringApplication.run(App.class, args);
  }
}
